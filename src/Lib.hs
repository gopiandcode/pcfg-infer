{-# LANGUAGE DeriveGeneric #-}
module Lib
    (
      Probabilistic, Grammar, Production ( RuleA, RuleB ), Derivation (DerivNT, DerivT), CFG,
      flatten_derivation, production_symbol, production_expand, lookup_symbol, retrieve_non_terminals,
      retrieve_non_terminals_for_symbol, retrieve_terminals, validate_symbols_distinct,
      validate_rule_a, check_rule_a, validate_rule_b, validate_probabilities, validate_grammar,
      uniform_grammar, weighted_select, sample_grammar_internal, sample_grammar, sample_grammar_n,
      calculate_terminal, calculate_non_terminal, retrieve_nt_prods, grammar_yield, learn_grammar,
      clear_grammar, inside_outside_internal, random_vec, sample_grammar_n_thresh, normalize_grammar
    ) where
import Prelude hiding (lookup)
import System.Random
import Control.Monad.State
import Data.Maybe
import Data.List hiding (insert,map, lookup)
import Data.HashMap.Strict hiding (map,filter, foldr)
import Data.Hashable
import GHC.Generics (Generic)

type Probabilistic = State StdGen


type Grammar = [(Production, Double)]

data Production = RuleA String String String
                | RuleB String String
                deriving (Show, Eq, Generic)

instance Hashable Production

data Derivation = DerivNT String Int Derivation Derivation
                | DerivT String Int String

instance Show Derivation where
  show = concat . flatten_derivation 

type CFG = [Either (String,String) (String, String, String)]

type ProbCache = HashMap (String, Int, Int) Double 

type Memoized = State (ProbCache,ProbCache)

type AlphaMem = State (HashMap (Either (String,Int,Int) Production) Double)


flatten_derivation :: Derivation -> [String]
flatten_derivation (DerivNT name id nt1 nt2) = concat . map flatten_derivation $ [nt1,nt2]
flatten_derivation (DerivT name id sym) = [sym]



production_symbol :: Production -> String
production_symbol (RuleA name _ _) = name
production_symbol (RuleB name _) = name


production_expand :: Production -> [String]
production_expand (RuleA _ a b) = [a,b]
production_expand (RuleB _ a) = [a]

lookup_symbol :: String -> Grammar -> [(Production,Double)]
lookup_symbol name [] = []
lookup_symbol name (x:xs) =
  if (name == production_symbol (fst x))
  then (x : (lookup_symbol name xs))
  else (lookup_symbol name xs )

retrieve_non_terminals :: Grammar -> [String]
retrieve_non_terminals ((RuleA symbol _ _,_):xs) = symbol:(retrieve_non_terminals xs)
retrieve_non_terminals ((RuleB symbol _,_):xs) = symbol:(retrieve_non_terminals xs)
retrieve_non_terminals [] = []

retrieve_nt_prods :: Grammar -> [(String,String,String)]
retrieve_nt_prods = nub . retr
               where retr ((RuleA symbol a b,_):xs) = (symbol, a, b):(retr xs)
                     retr (_:xs) = (retr xs)
                     retr [] = []

retrieve_nt_prods_full :: Grammar -> [(String,String,String, Production)]
retrieve_nt_prods_full = nub . retr
               where retr ((rule@(RuleA symbol a b),_):xs) = (symbol, a, b, rule):(retr xs)
                     retr (_:xs) = (retr xs)
                     retr [] = []

retrieve_t_prods_full :: Grammar -> [(String,String, Production)]
retrieve_t_prods_full = nub . retr
               where retr ((rule@(RuleB symbol a),_):xs) = (symbol, a, rule):(retr xs)
                     retr (_:xs) = (retr xs)
                     retr [] = []


retrieve_non_terminals_for_symbol :: Grammar -> String -> [String]
retrieve_non_terminals_for_symbol grammar symbol =
  nub . concat $ map (\(prod,_) -> case prod of
          RuleA sym nt1 nt2 -> if sym == symbol then [nt1, nt2] else []
          RuleB sym _       -> []
      ) grammar

retrieve_terminals :: Grammar -> [String]
retrieve_terminals ((RuleA symbol _ _,_):xs) = (retrieve_terminals xs)
retrieve_terminals ((RuleB symbol term,_):xs) = term:(retrieve_terminals xs)
retrieve_terminals [] = []

validate_symbols_distinct :: Grammar -> Bool
validate_symbols_distinct grammar = 
  all (\p -> (all (\ x -> x /= p) (retrieve_terminals grammar))) (retrieve_non_terminals grammar) &&
  all (\p -> (all (\ x -> x /= p) (retrieve_non_terminals grammar))) (retrieve_terminals grammar)


validate_rule_a ::  Grammar -> Bool
validate_rule_a grammar = all (validate_rule_a_pred grammar) grammar

validate_rule_a_pred grammar ((RuleA sybmol nt1 nt2,_)) = (any (== nt1) (retrieve_non_terminals grammar)) && (any (== nt2) (retrieve_non_terminals grammar))
validate_rule_a_pred grammar _ = True


check_rule_a :: Grammar -> [String]
check_rule_a grammar = concat $ map (check_rule_a_f grammar) grammar

check_rule_a_f grammar ((RuleA sybmol nt1 nt2,_)) =
  let fst = (if not ((any (== nt1) (retrieve_non_terminals grammar))) then [nt1] else []) in
  let snd = (if not ((any (== nt2) (retrieve_non_terminals grammar))) then [nt2] else []) in
    fst ++ snd
check_rule_a_f grammar _ = []

validate_rule_b :: Grammar -> Bool
validate_rule_b grammar@((RuleB sybmol t1,_):xs) = (not (any (== t1) (retrieve_non_terminals grammar))) && validate_rule_b xs
validate_rule_b (x:xs) = validate_rule_b xs
validate_rule_b [] = True

validate_probabilities :: Grammar -> Bool
validate_probabilities grammar = all (\sym -> abs (sum (map snd (lookup_symbol sym grammar)) - 1.0) < 0.01) (retrieve_non_terminals grammar)

validate_grammar :: Grammar -> Bool
validate_grammar grammar = validate_symbols_distinct grammar && validate_rule_a grammar && validate_rule_b grammar && validate_probabilities grammar

uniform_grammar :: [Production] -> Grammar
uniform_grammar productions = Prelude.map (\p -> (p,1.0)) productions

weighted_select :: (Num b, Ord b) => Int -> b -> [(a, b)] -> (a,Int)
weighted_select n pos ((ls, val):[]) = (ls,n)
weighted_select n pos ((ls, val):remain) =
  if pos <= val then (ls,n)
  else weighted_select (n + 1) (pos - val) remain

random_vec :: Int -> Probabilistic [Double]
random_vec 0 = return []
random_vec n = do
  gen <- get;
  let (choice,new_gen) = randomR (0, 1.0) gen;
  put new_gen;
  remain <- random_vec (n-1)
  return $ choice : remain

sample_grammar_internal :: Grammar -> String -> Probabilistic [String]
sample_grammar_internal grammar symbol = 
  let options = lookup_symbol symbol grammar in
    if length options > 0
    then do
      gen <- get;
      let (choice,new_gen) = randomR (0, sum (map snd options)) gen
      put new_gen
      let (new_samples, index) = weighted_select 0 choice options
      children <- mapM (sample_grammar_internal grammar) $ production_expand new_samples
      return $ concat children
      else return $ [symbol]


sample_grammar :: Grammar -> Probabilistic [String]
sample_grammar grammar = sample_grammar_internal grammar "start"

sample_grammar_n_thresh :: Int -> Grammar -> Double -> Probabilistic [[String]]
sample_grammar_n_thresh 0 grammar thresh = return $ []
sample_grammar_n_thresh n grammar thresh = do
  sample <- sample_grammar grammar;
  if grammar_yield grammar sample > thresh then do
    remain <- sample_grammar_n_thresh (n-1) grammar thresh;
    return $ sample: remain
  else (sample_grammar_n_thresh n grammar thresh)
    



sample_grammar_n :: Int -> Grammar -> Probabilistic [[String]]
sample_grammar_n 0 grammar = return $ []
sample_grammar_n n grammar = do
  sample <- sample_grammar grammar;
  remain <- sample_grammar_n (n-1) grammar;
  return $ sample: remain


calculate_terminal :: Grammar -> String -> String -> Double
calculate_terminal grammar symbol terminal =
  let options = lookup_symbol symbol grammar in
  let probs   = map snd $ filter (\(production,prob) -> case production of
             RuleA _ _ _ -> False
             RuleB _ term   -> term == terminal
         ) options in
    sum probs

calculate_non_terminal :: Grammar -> String -> String -> String -> Double
calculate_non_terminal grammar symbol nt1 nt2 =
  let options = lookup_symbol symbol grammar in
  let probs   = map snd $ filter (\(production,prob) -> case production of
             RuleA _ s1 s2 -> s1 == nt1 && s2 == nt2
             RuleB _ _   -> False
         ) options in
    sum probs

beta_lookup :: Lib.Grammar -> String -> Int -> Int -> State (HashMap (String, Int, Int) Double) Double
beta_lookup grammar symbol i j = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (symbol,i,j) state);
  return value

beta_update :: Lib.Grammar -> String -> Int -> Int -> (Double -> State (HashMap (String, Int, Int) Double) Double) -> State (HashMap (String, Int, Int) Double) ()
beta_update grammar symbol i j f = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (symbol,i,j) state);
  updated_value <- f value;
  put (insert (symbol, i, j) updated_value state)


initialize_terminals :: Lib.Grammar -> [String] -> State (HashMap (String, Int, Int) Double) (HashMap (String, Int, Int) Double)
initialize_terminals grammar text = do
  flip mapM [1..(length text)] $ \k ->
    flip mapM (nub (retrieve_non_terminals grammar)) $ \a -> do
         beta_update grammar a (k-1) k $ \v -> return (v + (calculate_terminal grammar a (text !! (k-1))))
  get


infer_probabilities grammar text = do
  let n = (length text)
  flip mapM [2..n] $ \width ->
    flip mapM [0..(n - width)] $ \i -> do
        let k = (i + width)
        flip mapM [i+1..(k-1)] $ \j -> do
          flip mapM (retrieve_nt_prods grammar) $ \(a,b,c) ->
            beta_update grammar a i k $ \bik -> do
                 let gabc = (calculate_non_terminal grammar a b c);
                 bij <- beta_lookup grammar b i j;
                 cjk <- beta_lookup grammar c j k;
                 return (bik + gabc * bij * cjk)
                 
-- calculates the probability    
grammar_yield :: Grammar -> [String] -> Double
grammar_yield grammar text = fst . flip runState empty $ do
  initialize_terminals grammar text;
  infer_probabilities grammar text;
  beta_lookup grammar "start" 0 (length text)

grammar_inside :: Grammar -> [String] -> (Double, HashMap (String, Int, Int) Double)
grammar_inside grammar text = fst . flip runState empty $ do
  initialize_terminals grammar text;
  infer_probabilities grammar text;
  value <- beta_lookup grammar "start" 0 (length text);
  state <- get;
  return $ (value, state)

    
  

  
alpha_lookup_value :: Lib.Grammar -> (String, Int, Int) -> AlphaMem Double
alpha_lookup_value grammar (symbol, i, j) = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (Left (symbol,i,j)) state);
  return value

alpha_lookup_rule :: Lib.Grammar -> Production -> State (HashMap (Either (String, Int, Int) (Production)) Double) Double
alpha_lookup_rule grammar production = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (Right production) state);
  return value

alpha_update_value :: Lib.Grammar -> (String,Int,Int) -> (Double -> AlphaMem Double) -> AlphaMem ()
alpha_update_value grammar (symbol,i,j) f = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (Left (symbol,i,j)) state);
  updated_value <- f value;
  put (insert (Left (symbol, i, j)) updated_value state)

alpha_update_rule :: Lib.Grammar -> Production -> (Double -> AlphaMem Double) -> AlphaMem ()
alpha_update_rule grammar production f = do
  state <- get;
  let value = fromMaybe (0.0) (lookup (Right production) state);
  updated_value <- f value;
  put (insert (Right production) updated_value state)

infer_alpha_probabilities :: Grammar -> HashMap (String, Int, Int) Double -> Int -> AlphaMem ()
infer_alpha_probabilities grammar beta_map n = do
  flip mapM [n, (n - 1) .. 2] $ \width ->

      flip mapM [0..(n - width)] $ \i ->

           let k = i + width in
           flip mapM [(i+1) .. (k - 1)] $ \j ->
           flip mapM (retrieve_nt_prods_full grammar) $ \(a,b,c,rule) -> do
             let beta_bij = fromMaybe (0.0) (lookup (b,i,j) beta_map);
             let beta_cjk = fromMaybe (0.0) (lookup (c,j,k) beta_map);
             let gabc = (calculate_non_terminal grammar a b c);

             alpha_update_rule grammar rule $ \v -> do
               alph_aik <- alpha_lookup_value grammar (a, i, k);
               return $ v + alph_aik * beta_bij * beta_cjk

             alpha_update_value grammar (b,i,j) $ \v -> do
               alph_aik <- alpha_lookup_value grammar (a, i, k);
               return $ v + gabc * alph_aik * beta_cjk

             alpha_update_value grammar (c,j,k) $ \v -> do
               alph_aik <- alpha_lookup_value grammar (a, i, k);
               return $ v + gabc * beta_bij * alph_aik

  return ()


infer_base_probabilities :: Grammar -> HashMap (String, Int, Int) Double -> [String] -> AlphaMem ()
infer_base_probabilities grammar beta_map text = do
  flip mapM [1..(length text)] $ \k ->
    flip mapM (nub (retrieve_non_terminals grammar)) $ \a -> do
         let wk = text !! (k - 1);
         alpha_update_rule grammar (RuleB a wk) $ \v -> do
               alph_akk <- alpha_lookup_value grammar (a,k-1,k);
               return $ v + alph_akk
  return ()

update_grammar :: Grammar -> Double -> AlphaMem Grammar
update_grammar grammar z =
  flip mapM grammar $ \(prod,prob) -> do
    alph_prod <- alpha_lookup_rule grammar prod;
    return $ (prod, alph_prod * prob / z)

inside_outside_internal :: Grammar -> [String] -> AlphaMem Grammar
inside_outside_internal grammar text = do
  let n = length text;
  let (z,beta_map) = grammar_inside grammar text;
  alpha_update_value grammar ("start",0,n) (\v -> return $ 1.0)
  infer_alpha_probabilities grammar beta_map (length text);
  infer_base_probabilities grammar beta_map text;
  update_grammar grammar z
  
inside_outside :: Grammar -> [String] -> Grammar
inside_outside grammar text = fst . flip runState empty $ (inside_outside_internal grammar text)

clear_grammar :: Grammar -> CFG
clear_grammar grammar = flip map grammar $ (\(prod,x) -> case prod of
                                                              RuleA a b c -> Right (a, b, c)
                                                              RuleB a b   -> Left (a, b)
                                           )

map_grammar :: CFG -> Grammar
map_grammar ls = flip map ls (\x -> case x of
                                 Left (a,b) -> (RuleB a b, 1.0)
                                 Right (a,b,c) -> (RuleA a b c, 1.0)
                             )

normalize_new_grammar :: Grammar -> Grammar
normalize_new_grammar grammar = flip map grammar $ \(prod,prob) ->
  let len = fromIntegral (length (lookup_symbol (production_symbol prod) grammar)) in (prod, 1.0/len)

normalize_grammar :: Grammar -> Grammar
normalize_grammar grammar = flip map grammar $ \(prod,prob) ->
  let prob_total = sum (map snd (lookup_symbol (production_symbol prod) grammar)) in
    if prob_total > 0.0 then
      (prod, prob/prob_total)
    else
      (prod, 0.0)


learn_grammar :: Int -> CFG -> [[String]] -> Grammar
learn_grammar 0 cfg text = normalize_new_grammar . map_grammar $ cfg
learn_grammar n cfg text_samples = foldr (\text grammar -> normalize_grammar (inside_outside  grammar text)) (learn_grammar (n-1) cfg text_samples) text_samples
     

