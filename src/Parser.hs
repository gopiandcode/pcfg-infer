{-# LANGUAGE RecursiveDo #-}
module Parser  where
import Prelude hiding (lookup)
import System.Random
import Control.Monad.State
import Control.Applicative hiding (empty)
import Data.Functor
import Data.Maybe
import Data.List hiding (insert,map, lookup)
import Data.HashMap.Strict hiding (map,filter)
import Text.Earley.Grammar as Earley
import Text.Earley.Mixfix as Mixfix
import Text.Earley.Parser
import Text.Earley  (namedToken)
import Control.Monad.Fix
import Lib

example_grammar_earley :: Earley.Grammar r (Prod r String String Derivation)
example_grammar_earley = mdo
  start <- rule $ DerivNT "start" 0 <$> statement <*> statement_star;
  statement_star <- rule $ (DerivNT "statement*" 0 <$> statement_end <*> statement_star1)
                        <|> (DerivT "statement*" 1 <$> namedToken ";\n");


  statement_star1 <- rule $ DerivNT "statement*1" 0 <$> statement <*> statement_star;
  statement_end   <- rule $ DerivT "statementEnd" 0 <$> namedToken ";\n";
  statement <- rule $ DerivNT "statement" 0 <$> print_expr1 <*> print_expr2
                   <|> DerivNT "statement" 1 <$> assign_expr1 <*> assign_expr2;

  print_expr1 <- rule $ DerivT "print_expr1" 0 <$> namedToken "PRINT";
  print_expr2 <- rule $ DerivNT "print_expr2" 0 <$> print_expr3 <*> statement_star;
  print_expr3 <- rule $ DerivT "print_expr3" 0 <$> namedToken "LABEL";

  assign_expr1 <- rule $ DerivT "assign_expr1" 0 <$> namedToken "LABEL";
  assign_expr2 <- rule $ DerivNT "assign_expr2" 0 <$> assign_expr3 <*> assign_expr4;
  assign_expr3 <- rule $ DerivT "assign_expr3" 0 <$> namedToken ":=";
  assign_expr4 <- rule $ DerivNT "assign_expr4" 0 <$> expr <*> statement_star
                      <|> DerivNT "assign_expr4" 1 <$> const <*> statement_star;
  const <- rule $ DerivT "const" 0 <$> namedToken "CONST";

  expr <- rule $ DerivNT "expr" 0 <$> expr_add1 <*> expr
              <|> DerivT "expr" 1 <$> namedToken "CONST"
              <|> DerivT "expr" 2 <$> namedToken "LABEL";

  expr_add1 <- rule $ DerivNT "expr_add1" 0 <$> expr <*> expr_add2;
  expr_add2 <- rule $ DerivT "expr_add2" 0 <$> namedToken "+";

  return $ start


-- parses a sampled expression into the set of possible parses
example_grammar_parser :: [String] -> [Derivation]
example_grammar_parser = fst . fullParses (parser example_grammar_earley)

