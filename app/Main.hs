module Main where
import Prelude hiding (lookup)
import System.Random
import Control.Monad.State
import Control.Applicative hiding (empty)
import Data.Functor
import Data.Maybe
import Data.List hiding (insert,map, lookup)
import Data.HashMap.Strict hiding (map,filter)
import Control.Monad.Fix
import Lib




example_grammar :: Lib.Grammar
example_grammar = [
  (RuleA "start" "statement" "statement*", 1.0),
  (RuleA "statement*" "statementEnd" "statement*1", 0.3),
  (RuleB "statement*" ";\n", 0.7),

  (RuleA "statement*1" "statement" "statement*", 1.0),
  (RuleB "statementEnd" ";\n", 1.0),

  (RuleA "statement" "print_expr1" "print_expr2", 0.5),
  (RuleB "print_expr1" "PRINT", 1.0),
  (RuleA "print_expr2" "print_expr3" "statement*", 1.0),
  (RuleB "print_expr3" "LABEL", 1.0),

  (RuleA "statement" "assign_expr1" "assign_expr2", 0.5),
  (RuleB "assign_expr1" "LABEL", 1.0),
  (RuleA "assign_expr2" "assign_expr3" "assign_expr4", 1.0),
  (RuleB "assign_expr3" ":=", 1.0),
  (RuleA "assign_expr4" "expr" "statement*", 0.5),
  (RuleA "assign_expr4" "const" "statement*", 0.5),
  (RuleB "const" "CONST", 1.0),

  (RuleA "expr" "expr_add1" "expr", 0.3),
  (RuleB "expr" "CONST", 0.35),
  (RuleB "expr" "LABEL", 0.35),
  (RuleA "expr_add1" "expr" "expr_add2", 1),
  (RuleB "expr_add2" "+", 1)
                 ]


-- generates a pseudo-random expression from the language given an initial value
get_example_expression :: Int -> [String]
get_example_expression n = fst $ flip runState (mkStdGen n) $ sample_grammar example_grammar 

print_example_expression :: Int -> IO ()
print_example_expression n = putStrLn $ concat $ fst $ flip runState (mkStdGen n) $ sample_grammar example_grammar 

print_grammar_expression :: Int -> Grammar -> IO ()
print_grammar_expression n grammar = putStrLn $ concat $ fst $ flip runState (mkStdGen n) $ sample_grammar grammar 


print_n_example_expression :: Int -> Int -> IO ()
print_n_example_expression n count = putStrLn $ concat $ concat $ fst $ flip runState (mkStdGen n) $ sample_grammar_n count example_grammar 

examples = fst . flip runState (mkStdGen 14) $  sample_grammar_n_thresh 10 example_grammar 0.18

example_grammar_cfg = clear_grammar example_grammar

learned_grammar = learn_grammar 3 example_grammar_cfg examples

main :: IO ()
main = do
  -- print the learned grammar rules
  mapM (putStrLn . show) learned_grammar
  -- print an expression from the learned grammar
  print_grammar_expression 10 learned_grammar
  -- show the training examples
  mapM (putStrLn . show) examples
  return ()

